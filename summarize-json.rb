#!/usr/bin/ruby -w

require 'json'
require 'pp'
require 'date'

s = STDIN.read
d = JSON::parse(s)

$d = {}
(1...d['data'].length).each do |i|
  (0...d['data'][0].length).each do |j|
    release = d['releases'][i-1]
    date = d['data'][0][j]
    $d[date] ||= {}
    $d[date][release] ||= {}
    $d[date][release] = d['data'][i][j]
  end
end
$releases = d['releases']
$releases.sort!
#do |a, b|
#  puts("dpkg --compare-versions #{a} lt #{b}")
#  system("dpkg --compare-versions #{a} lt #{b}")
#  break 1 if $?.exitstatus == 0
#  break -1
#end

$r = {}
$d.keys.sort.each do |d|
  next if d < '2007-01-01'
#  next if not d =~ /01$/
  $r[d] = {}
  if d < '2007-04-08' # before etch
    # popcon version in stable/sarge: 1.28
    $r[d]['sarge'] = $d[d]['1.28']
    $r[d]['testing'] = $d[d].select { |a, b| a > '1.28' }.inject(0) { |a, b| a+b[1] } - $d[d]['unknown']
    $r[d]['other'] =  $d[d].inject(0) { |a, b| a+b[1] } - $r[d]['sarge'] - $r[d]['testing']
  elsif d < '2009-02-14' # before lenny
    # popcon version in stable/etch: 1.41
    $r[d]['sarge'] = $d[d]['1.28']
    $r[d]['etch'] = $d[d]['1.41']
    $r[d]['testing'] = $d[d].select { |a, b| a > '1.41' }.inject(0) { |a, b| a+b[1] } - $d[d]['unknown']
    $r[d]['other'] =  $d[d].inject(0) { |a, b| a+b[1] } - $r[d]['sarge'] - $r[d]['etch'] - $r[d]['testing']
  elsif d < '2011-02-06' # before squeeze
    # popcon version in stable/lenny: 1.46
    $r[d]['sarge'] = $d[d]['1.28']
    $r[d]['etch'] = $d[d]['1.41']
    $r[d]['lenny'] = $d[d]['1.46']
    $r[d]['testing'] = $d[d].select { |a, b| a > '1.46' }.inject(0) { |a, b| a+b[1] } - $d[d]['unknown']
    $r[d]['other'] =  $d[d].inject(0) { |a, b| a+b[1] } - $r[d]['sarge'] - $r[d]['etch'] - $r[d]['lenny'] - $r[d]['testing']
  elsif d < '2013-05-05' # before wheezy
    # popcon version in stable/squeeze: 1.49
    $r[d]['sarge'] = $d[d]['1.28']
    $r[d]['etch'] = $d[d]['1.41']
    $r[d]['lenny'] = $d[d]['1.46']
    $r[d]['squeeze'] = $d[d]['1.49']
    $r[d]['testing'] = $d[d].select { |a, b| a > '1.49' }.inject(0) { |a, b| a+b[1] } - $d[d]['unknown']
    $r[d]['other'] =  $d[d].inject(0) { |a, b| a+b[1] } - $r[d]['sarge'] - $r[d]['etch'] - $r[d]['lenny'] - $r[d]['squeeze'] - $r[d]['testing']
  else
    # popcon version in stable/wheezy: 1.56
    $r[d]['sarge'] = $d[d]['1.28']
    $r[d]['etch'] = $d[d]['1.41']
    $r[d]['lenny'] = $d[d]['1.46']
    $r[d]['squeeze'] = $d[d]['1.49']
    $r[d]['wheezy'] = $d[d]['1.56']
    $r[d]['testing'] = $d[d].select { |a, b| a > '1.56' }.inject(0) { |a, b| a+b[1] } - $d[d]['unknown']
    $r[d]['other'] =  $d[d].inject(0) { |a, b| a+b[1] } - $r[d]['sarge'] - $r[d]['etch'] - $r[d]['lenny'] - $r[d]['squeeze'] - $r[d]['wheezy'] - $r[d]['testing']
  end
end
puts "time sarge etch lenny squeeze wheezy testing/unstable other"
$r.keys.sort.each do |d|
  puts "#{d} #{$r[d]['sarge'] || 0 } #{$r[d]['etch'] || 0 } #{$r[d]['lenny'] || 0 } #{$r[d]['squeeze'] || 0 } #{$r[d]['wheezy'] || 0 } #{$r[d]['testing'] || 0 } #{$r[d]['other'] || 0 }"
end
